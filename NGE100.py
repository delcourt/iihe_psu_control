
import pyvisa
import psu_common
import time,os

VOLT_LIMITS = [0,15]

class NGE100:
    def __init__(self,tty= None, serial_number = None):
        self.inst = psu_common.open_device(tty)
        if self.inst == None:
            print("Fatal error, unable to connect to PSU !")
        else:
            self.allow_local()
    def get_id(self):
        print(self.query("*IDN?"))

    def query(self,command):
        return self.inst.query(command).replace("\x11","").replace("\r","").replace("\n","").replace("\x13","")
        #if not self.wait_unlock():
        #    return 0
        #self.lock()
        #self.unlock()
        #return output

    def write(self,command):
        self.inst.write(command)
        return 1
        if not self.wait_unlock():
            return 0
        self.lock()
        self.unlock()


    def wait_unlock(self):
        t0 = time.time()

        while LOCK_FILE in os.listdir("."):
            print("Waiting...")
            t1 = time.time()
            time.sleep(0.1)
            if t1-t0 > 5:
                print("COMMAND TIMEOUT !")
                return 0
        return 1

    def set_channel(self,channel):
        channel = int(channel)
        self.write(f"INST:NSEL {channel}")
        if int(self.query("INST:NSEL?")) != channel:
            print(f"ERROR unable to get channel ({channel})")

    def get_voltage(self, channel = 0):
        #Select channel:
        if channel != 0:
            self.set_channel(channel)
        return self.query("MEAS:VOLT?")

    def get_current(self, channel = 0):
        #Select channel:
        if channel != 0:
            self.set_channel(channel)
        return self.query("MEAS:CURR?")

    def allow_local(self):
        self.write("SYSTem:MIX")
    def set_local(self):
        self.write("SYSTem:LOCal")

    def is_enabled(self,channel = 0):
        if channel != 0:
            self.set_channel(channel)
        return int(self.query("OUTP:STAT?"))

    def turn_on(self, channel = 0):
        if channel != 0:
            self.set_channel(channel)
        self.write("OUTP:STAT 1")

    def turn_off(self, channel = 0):
        if channel != 0:
            self.set_channel(channel)
        self.write("OUTP:STAT 0")

    def get_v_set(self, channel = 0):
        if channel != 0:
            self.set_channel(channel)
        return(float(self.query("VOLT?")))

    def set_v(self, volt, channel = 0):
        if channel != 0:
            self.set_channel(channel)
        volt = float(volt)
        print(f"{volt}")
        if volt < VOLT_LIMITS[0] or volt > VOLT_LIMITS[1]:
            raise "Voltage ({volt}) out of bounds !"
        self.write(f"VOLT {volt}")


def menu(question, options):
    print(question)
    opt_str = "Options : ["
    for o in options:
        opt_str+=f" '{o}'"
    opt_str+=" ]"
    print(opt_str)
    opt_ll = [o.lower() for o in options]
    xxx = input()
    while not xxx.lower() in opt_ll:
        print(f"'{xxx}' not in {opt_str}")
        xxx = input()
    return xxx.lower()

def get_float(question):
    succeeded = False
    while not succeeded:
        print(question)
        print("Please enter a float")
        dd = input()
        try:
            dd = float(dd)
        except:
            pass
    return dd



if __name__=="__main__":
    import zmq_interface
    #psu_common.list_devices()
    lv_psu = NGE100(tty="ASRL/dev/ttyACM0::INSTR")
    #zmq_interface.zmq_server({"0":NGE100(tty="ASRL/dev/ttyACM0::INSTR")})