import zmq

class zmq_server:
  def __init__(self,target_objects, port = 5555):
    print(f"Launching zmq_server on port {port}")
    self.obj  = target_objects
    self.port = port
    self.run()
  
  def run(self):
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind(f"tcp://*:{self.port}")
    while True:
      message = socket.recv().decode("utf-8")
      print("Received request: %s" % message)
      socket.send(bytes(self.process_message(message),"utf-8"))

  def process_message(self,message):
    try:
      if message.lower() == "ping":
          return("pong")
      elif message.lower() == "list_objects":
          return(f"{self.obj}")
      elif message.split(" ")[0] in self.obj.keys():
          message = message.split(" ")
          target_obj = self.obj[message[0]]
          if len(message) == 1:
            return("pong")
          elif message[1] in target_obj.__class__.__dict__.keys():
            if len(message) == 2:
              return str(target_obj.__class__.__dict__[message[1]](target_obj))
            else:
              return str(target_obj.__class__.__dict__[message[1]](target_obj,*message[2:]))
          else:
            return(f"error, {message[1]} not a method of {message[0]}")
      else: 
        return(f"error : {message.split(' ')[0]} not a recognized object label")
    except Exception as e:
      return(f"error : {e}")


class zmq_client:
    def __init__(self, port = 5555, verbose = False):
      context = zmq.Context()
      if verbose:
        print(f"Connecting to zmq server at port {port}…")
      self.socket = context.socket(zmq.REQ)
      self.socket.connect(f"tcp://localhost:{port}")
      self.connected = False
      if self.query("ping") == "pong":
          if verbose:
            print("Done!")
          self.connected = True
      else:
          print("Error, unable to connect to server !")

    def is_connected(self):
      return self.connected

    def query(self,message):
        self.socket.send(bytes(message,'utf-8'))
        return self.socket.recv().decode("utf-8")



if __name__ == "__main__":
  from random import random
  class dummy_psu:
    def __init__(self, id):
      self.id = id
    def ping(self, msg = ""):
      return (f"pong {msg}")
    def two_args (self, arg1, arg2):
      return (f"{arg1} {arg2}")
    def get_voltage(self, channel):
      channel = int(channel)
      return 0.1*random() + channel

    def get_current(self, channel):
      channel = int(channel)
      return 10*random() + 100*channel

  psu_list = {"0" : dummy_psu(0)}
  zmq_server(psu_list)
