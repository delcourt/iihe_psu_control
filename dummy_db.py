from os import path
from os import SEEK_END

class dummy_db:
    def __init__(self,file_name = "",structure=None):
        self.file_name = file_name
        self.f = None


        self.event_length = 0
        self.temp_data = None
        self.index = None
        self.data = []
        
        self.n_events  = 0
        self.start_pos = 0
        self.stop_pos  = 0

        self.structure = []
        if not structure == None:
            for field in structure:
                self.event_length+=field[1]
                self.structure.append((field[0],field[1]))

        if path.exists(self.file_name):
            self.refresh_from_file()

    def refresh_from_file(self):
        if self.f:
            self.f.close()

        buffer = None
        self.f=open(self.file_name,"rb")
        c = "."
        while c and ord(c) != 35:
            c = self.f.read(1)
            if buffer == None:
                buffer = c
            else:
                buffer+=c
        buffer = str(buffer)[2:-2].split(";")

        self.structure = []
        self.event_length = 0
        for entry in buffer:
            name,size = entry.split(":")
            self.structure.append((name,int(size)))
            self.event_length+=int(size)
        self.get_index()

        self.start_pos = self.f.tell()
        self.stop_pos = self.f.seek(0,SEEK_END)
        self.f.seek(self.start_pos)
        
        self.number_of_events = (self.stop_pos - self.start_pos)/self.event_length
        if not self.number_of_events == int(self.number_of_events):
            print ("Error, number of event is not an int. Corrupted file or bad header?")
        self.number_of_events = int(self.number_of_events)

    def get_index(self):
        self.index = {}
        if self.structure == None:
            return
        pos = 0
        for name,size in self.structure:
            self.index[name]=(size,pos)
            pos+=size

    def add(self,entry):
        if self.index==None:
            self.get_index()
        to_add = 0

        for key,value in entry.items():
            size,pos = self.index[key]
            is_neg = value < 0
            if is_neg:
                value = value * (-1)
            to_add |= (value%(1<<(8*size-1)))<<8*(self.event_length-pos-size)
            if is_neg:
                pos_to_add = 8*(self.event_length-pos)-1
                to_add |= 1<<(pos_to_add)
        self.data.append(to_add)

    def write_to_file(self):
        if self.f:
            self.f.close()
        if path.exists(self.file_name):
            with open(self.file_name, "ab") as f:
                for d in self.data:
                    f.write(d.to_bytes(self.event_length,"big"))
        else:
            print("Creating new file")
            with open(self.file_name,"wb") as f:
                #Write header
                header = ""
                for name,size in self.structure:
                    header+=f"{name}:{size};"
                header = header[:-1]+"#"
                f.write(str.encode(header))
                #Write data
                for d in self.data:
                    f.write(d.to_bytes(self.event_length,"big"))
        self.data = []
        self.refresh_from_file()

    def get_entry(self,position):
        self.move_to(position)
        return self.read_event()

    def read_event(self):
        if self.f == None:
            self.refresh_from_file()
            print("Error, file is not opened...")
            print("To be implemented")
        c = int.from_bytes(self.f.read(self.event_length),byteorder='big')
        if c == 0:
            return None
        if self.index==None:
            self.get_index()
        event = {}
        for name, (size,pos) in self.index.items():
            event[name] = (c>>(8*(self.event_length-pos-size)))%(1<<(8*(size)-1))
            if (c>>(8*(self.event_length-pos)-1))%(2):
                event[name] *= -1
        return(event)

    def move_to(self,event_position):

        if event_position >= 0:
            pointer_pos = self.start_pos + event_position*self.event_length
        else:
            pointer_pos = self.stop_pos + event_position*self.event_length

        if pointer_pos > self.stop_pos or pointer_pos < self.start_pos:
            print("Error, unable to move to event #{}".format(event_position))
        else:
            self.f.seek(pointer_pos)

    def skip(self,number_of_events):
        number_of_events = int(number_of_events)
        if not self.f:
            print("Opening file...")
            self.f.refresh_from_file()
        new_pos = self.f.tell() + number_of_events*self.event_length
        if new_pos < self.start_pos:
            print("Setting back to first entry")
            new_pos = self.start_pos
        elif new_pos > self.stop_pos:
            print("Warning, reached the end of the file")
        self.f.seek(new_pos)




if __name__ == "__main__":
    #dd = dummy_db(file_name = "new.db", structure = [('date', 4), ('temp', 2), ('hum', 2)])
    dd = dummy_db(file_name = "new.db", structure = [('date', 4), ('temp', 2), ('hum', 2)])
    for i in range(1000):
        #event = {"date":i,"temp":i,"hum":i}   
        print(dd.read_event())
        dd.skip(9)


    #dd.write_to_file()
    print(dd.number_of_events)
    #dd.move_to(-1)
    #print (dd.read_event())
