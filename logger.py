from zmq import DELAY_ATTACH_ON_CONNECT
from dummy_db import dummy_db
import time
#from HMP4040 import HMP4040
from zmq_interface import zmq_client
import os
ZMQ_PORT = 5555   #Port of the ZMQ HW interface service
DELAY    = 5      #Time between measurements

db = dummy_db(file_name = "psu.db", structure = [('date', 4), ('LV1', 2), ('LV2', 2),('LV3', 2), ('LV4', 2), ('LI1', 2), ('LI2', 2),('LI3', 2), ('LI4', 2)])

#Connecting to hw interface service
rq = zmq_client(ZMQ_PORT)
if not rq.is_connected():
  print(f"Unable to connect to zmq server at port {ZMQ_PORT}")

while True:
    t0 = time.time()    
    try:
        entry = {}
        entry["date"] = int(time.time())
        for i in range(1,4):
            entry[f"LV{i}"] = int(1000*float(rq.query(f"0 get_voltage {i}"))+0.5)
            entry[f"LI{i}"] = int(1000*float(rq.query(f"0 get_current {i}"))+0.5)
        db.add(entry)
        db.write_to_file()
    except Exception as e:
        print(f"Logger error : {e}")
    dt = time.time()-t0
    if dt < DELAY:
      time.sleep(DELAY - dt)
    
