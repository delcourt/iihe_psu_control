import zmq_interface
import psu_common
from Dummy_psu import Dummy_psu
from DT54xx import DT54xx
from NGE100 import NGE100

devices = psu_common.list_devices()
#zmq_interface.zmq_server({"0":NGE100(tty="ASRL/dev/ttyACM1::INSTR")})


#0 = LV
#1 = HV
devs = {"0" : Dummy_psu(), "1" : Dummy_psu()}

for dev in devices:
    if len(dev) != 4:
        print("Error, device is not 4 entry long !")
    name = dev[2]
    if name == "NGE103B":
        print("Found R&S LV PSU")
        devs["0"] = NGE100(tty=dev[0])
    elif name == "DT5471":
        print("Found CAEN HV PSU")
        devs["1"] = NGE100(tty=dev[0])

zmq_interface.zmq_server(devs)