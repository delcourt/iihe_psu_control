
class Dummy_psu:
    def __init__(self,tty= None, serial_number = None):
        pass
    def query(self,command):
        pass
                            
    def write(self,command):
        pass

    def get_id(self):
        return("dummy")
    
    def get_voltage(self, channel = 0):
        return -1

    def get_current(self, channel = 0):
        return -1
        
    def allow_local(self):
        return("ERR : This command isn't possible on this PSU")
        
    def set_local(self):
        return("ERR : This command isn't possible on this PSU")

    def is_enabled(self,channel = 0):
        self.channel_check(channel)
        return -1

    def turn_on(self, channel = 0):
        return("ERR : This command isn't possible on this PSU")

    def turn_off(self, channel = 0):
        return("ERR : This command isn't possible on this PSU")

    def get_v_set(self, channel = 0):
        return(-1)
        
    def set_v(self, volt, channel = 0):
        return("ERR : This command isn't possible on this PSU")
