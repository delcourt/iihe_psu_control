
import pyvisa
import psu_common
import time,os

VOLT_LIMITS = [0,900]

class DT54xx:
    def __init__(self,tty= None, serial_number = None):
        self.inst = psu_common.open_device(tty)
        if self.inst == None:
            print("Fatal error, unable to connect to PSU !")


    def query(self,command):
        r_str=self.inst.query(command).replace("\x11","").replace("\r","").replace("\n","").replace("\x13","")
        r_str = r_str.split(",")
        if len(r_str)==0:
            return("ERR : No answer from device")
        elif len(r_str) == 1:
            if "CMD:" in r_str[0]:
                return r_str[0].split("CMD:")[1]
            else:
                return (f"ERR : return value is : {r_str}")
        elif len(r_str) == 2:
            if "CMD:OK" in r_str[0] and "VAL:" in r_str[1]:
                return r_str[1].split("VAL:")[1]
            else:
                return (f"ERR : return value is : {r_str}")

                            

    def write(self,command):
        self.inst.write(command)
        return 1

    def get_id(self):
        return(self.query("$CMD:MON,PAR:BDNAME\r"))

    def channel_check(self,channel):
        if channel != 0:
            print(f"Warning, trying to access unexisting channel {channel}")
    
    def get_voltage(self, channel = 0):
        self.channel_check(channel)
        return self.query("$CMD:MON,PAR:VMON\r")

    def get_current(self, channel = 0):
        self.channel_check(channel)
        return self.query("$CMD:MON,PAR:IMON\r")
        
    def allow_local(self):
        return("ERR : This command isn't possible on this PSU")
        
    def set_local(self):
        return("ERR : This command isn't possible on this PSU")

    def is_enabled(self,channel = 0):
        self.channel_check(channel)
        return int(self.query("OUTP:STAT?"))

    def turn_on(self, channel = 0):
        self.channel_check(channel)
        return self.query("$CMD:SET,PAR:ON\r")

    def turn_off(self, channel = 0):
        self.channel_check(channel)
        return self.query("$CMD:SET,PAR:OFF\r")

    def get_v_set(self, channel = 0):
        self.channel_check(channel)
        return(float(self.query("$CMD:MON,PAR:VSET\r")))
        
    def set_v(self, volt, channel = 0):
        self.channel_check(channel)

        volt = float(volt)
        if volt < VOLT_LIMITS[0] or volt > VOLT_LIMITS[1]:
            raise "Voltage ({volt}) out of bounds !"
        return (self.query(f"$CMD:SET,PAR:VSET,VAL:{volt}\r"))


if __name__=="__main__":    
    #import zmq_interface
    #zmq_interface.zmq_server({"0":NGE100(tty="ASRL/dev/ttyACM1::INSTR")})
    #psu_common.list_devices()
    hv_psu = DT54xx(tty="ASRL/dev/ttyACM0::INSTR")

    print(hv_psu.get_id())
    print(hv_psu.set_v(100))
    print(hv_psu.get_v_set())
    print(hv_psu.get_voltage())
    print(hv_psu.get_current())
    print(hv_psu.turn_on())
    print(hv_psu.turn_off())